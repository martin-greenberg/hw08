/*
 * ObjectFiles.h
 */

#include "LC4.h"

// Read an object file and modify the machine state accordingly
// Return a zero if successful and a non-zero error code if you encounter a problem
int ReadObjectFile (char *filename, MachineState *theMachineState);

int isBigEndian(FILE* obj); //returns 1 if obj is big endian, 0 otherwise
int isSectionMarker(unsigned short int i); //returns 1 if i is a section marker
unsigned short int getWord (FILE* obj, int endian); //returns the next word in obj, formatted for endianness