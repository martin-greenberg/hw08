//
//  trace.c
//  LC4
//
//  Created by Martin Greenberg on 11/19/13.

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "ObjectFiles.h"
#define R(I) (((I) >> 10) & 0x1F) //shift right to get the upper 6 bits then bitwise AND with 11111 to get I[14:10]
#define G(I) (((I) >> 5) & 0x1F) //bitwise AND of I shifted right by 5 with 11111 to get I[9:5]
#define B(I) ((I) & 0x1F) //bitwise AND of I and 11111 to get I[4:0]
#define INST_15_8(I) ((I) >> 8)
#define INST_7_0(I) ((I) & 0xFF)


int main(int argc, char * argv[]){
	int status = 0;
	int bytes_written = 0;
	if(argc == 1){
        fprintf(stderr,"no object files passed as arguments");
        return 1;
    }
    
    FILE* trace_out = fopen("trace_out.txt", "w");
    
    ControlSignals *theControls;
    theControls = (ControlSignals *) malloc(sizeof(ControlSignals));
    
    MachineState *theMachineState;
    theMachineState = (MachineState *) malloc(sizeof(MachineState));
    
    DatapathSignals *theDatapath;
    theDatapath = (DatapathSignals *) malloc(sizeof(DatapathSignals));
    
    Reset(theMachineState);
    
    for(int i = 1; i < argc; i++){
        ReadObjectFile(argv[i],theMachineState);
    }
    
    while(theMachineState->PC != 0x80FF){
		fputc(INST_7_0(theMachineState->PC),trace_out); // write the PC
		fputc(INST_15_8(theMachineState->PC),trace_out);
		fputc(INST_7_0(theMachineState->memory[theMachineState->PC]),trace_out);
		fputc(INST_15_8(theMachineState->memory[theMachineState->PC]),trace_out); //write the current instruction
		bytes_written += 4;
		status = UpdateMachineState(theControls,theMachineState,theDatapath);
		if(status != 0){
			fprintf(stderr,"UpdateMachineState exited with error code %i.\n",status);
			fflush(trace_out);
			ftruncate(fileno(trace_out), bytes_written - 4); //if the last instruction went bad, truncate the trace to remove it
			fclose(trace_out); //remember to free all allocations in case of error, to prevent memory leak
			free(theMachineState);
			free(theControls);
			free(theDatapath);
			return status;
		}
    }
    
    //write the stuff once more to capture the last instruction performed
    fputc(INST_7_0(theMachineState->PC),trace_out); // write the PC
	fputc(INST_15_8(theMachineState->PC),trace_out);
	fputc(INST_7_0(theMachineState->memory[theMachineState->PC]),trace_out);
	fputc(INST_15_8(theMachineState->memory[theMachineState->PC]),trace_out); //write the current instruction
	
    FILE* img_out = fopen("image.ppm", "w");
	
    fputs("P6\n", img_out); //magic number
    fputs("# Output from Martin's trace program in ppm format\n", img_out);
    fputs("128\t", img_out); //width: 128 columns
    fputs("124\n", img_out); //height: 124 rows
    fputs("31\n",img_out); //maxval of color is 255 for each of RGB
    
    for(int y = 0; y<124; y++){
        for(int x = 0; x<128; x++){
            unsigned int pixcont = theMachineState->memory[49152+x+(y*128)];
            fputc((char)R(pixcont),img_out);
            fputc((char)G(pixcont),img_out);
            fputc((char)B(pixcont),img_out);
        }
    }
    fclose(img_out);
    fclose(trace_out);
    free(theControls);
    free(theMachineState);
    free(theDatapath);
}
