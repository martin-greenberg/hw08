//
//  ObjectFiles.c
//  LC4
//
//  Created by Martin Greenberg on 11/19/13.
//  Copyright (c) 2013 Martin Greenberg. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include "ObjectFiles.h"

#define CODE 0xCADE
#define DATA 0xDADA
#define SYMBOL 0xC3B7
#define FILEN 0xF17E
#define LINENUM 0x715E

int isBigEndian(FILE *obj){
    unsigned short int c1 = fgetc(obj);
    unsigned short int c2 = fgetc(obj);
    
    while(!feof(obj)){
        switch((c1<<8)|c2){
            case CODE:
                rewind(obj);
                return 0;
            case SYMBOL:
                rewind(obj);
                return 0;
            case FILEN:
                rewind(obj);
                return 0;
            case LINENUM:
                rewind(obj);
                return 0;
            default:
                c1 = c2;
                c2 = fgetc(obj);
                break;
        }
    }
    rewind(obj);
    return 1;
}

unsigned short int getWord (FILE* obj, int endian){
    if(endian){
        return ((fgetc(obj)) | (fgetc(obj) << 8));
    } else {
        return ((fgetc(obj) << 8) | (fgetc(obj)));
    }
}

int isSectionMarker(unsigned short int i){
    return((i == CODE) || (i == DATA) || (i == SYMBOL) || (i == FILEN) || (i == LINENUM));
}

int ReadObjectFile(char *filename, MachineState *theMachineState){
    FILE *obj = fopen(filename,"r");
    if(obj == NULL){
        fprintf(stderr, "Could not read file.\n");
        return 1;
    }
    unsigned short int currInst = 0;
    int endian = isBigEndian(obj);
    
    unsigned short int addr = 0;
    unsigned short int len = 0;
    fscanf(obj,"%hx",&currInst);
    currInst = getWord(obj,endian);
    
    while(!feof(obj)){
        switch(currInst){
            case CODE:
                addr = getWord(obj,endian);
                
                len = getWord(obj,endian);
                
                currInst = getWord(obj,endian);
                
                for(int i=0;i<len;i++){
                    theMachineState->memory[addr] = currInst;
                    addr = addr + 1;
                    
                    currInst = getWord(obj,endian);
                }
                break;
            case DATA:
                addr = getWord(obj,endian);
                
                len = getWord(obj,endian);
                
                currInst = getWord(obj,endian);
                
                for(int i=0;i<len;i++){
                    theMachineState->memory[addr] = currInst;
                    addr = addr + 1;
                    
                    currInst = getWord(obj,endian);
                }
                break;
            case SYMBOL:
                currInst = getWord(obj,endian);
                
                len = getWord(obj,endian);
                
                for(int i=0;i<len;i++){
                    getc(obj);
                }
                break;
            case FILEN:
                len = getWord(obj,endian);
                
                for(int i=0;i<len;i++){
                    getc(obj);
                }
                break;
            case LINENUM:
                getWord(obj,endian); //addr
                getWord(obj,endian); //line
                getWord(obj,endian); //file index
                break;
            default:
                currInst = getWord(obj,endian);
                addr++;
                break;
        }
    }
    fclose(obj);
    return 0;
}