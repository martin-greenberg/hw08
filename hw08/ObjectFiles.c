//
//  LC4.c
//  LC4
//
//  Created by Martin Greenberg on 11/18/13.

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "LC4.h"
#define INSN_OP(I) ((I) >> 12)
#define INSN_11_9(I) (((I)>>9) & 0x7)
#define INSN_8_6(I)  (((I)>>6) & 0x7)
#define INSN_SUBOP(I) (((I)>>3) & 0x7)
#define INSN_2_0(I) (I & 0x7)
#define UIMM9(I) (I & 0x1FF)
#define UIMM5(I) (I & 0x1F)
#define UIMM6(I) (I & 0x3F)
#define UIMM4(I) (I & 0xF)
#define UIMM8(I) (I & 0xFF)
#define UIMM7(I) (I & 0x7F)
#define UIMM11(I) (I & 0x7FF)
#define INSN_8_7(I) (((I)>>7) & 0x3)
#define INSN_5(I) (((I)>>5) & 0x1)
#define INSN_11(I) (((I)>>11) & 0x1)
#define INSN_5_4(I) (((I)>>4) & 0x3)
#define INSN_12(I) (((I)>>12) & 0x1)
#define NZP(I) ((I << 13) >> 13)

int DecodeCurrentInstruction(unsigned short int INSN, ControlSignals *theControls){
    switch(INSN_OP(INSN)){
        case 0x0:  //NOP or BR
            switch(INSN_11_9(INSN)){
                case 0x0:  //NOP
                    theControls->PCMux_CTL = 1;
                    theControls->rsMux_CTL = 0;
                    theControls->rtMux_CTL = 0;
                    theControls->rdMux_CTL = 0;
                    theControls->regFile_WE = 0;
                    theControls->regInputMux_CTL = 0;
                    theControls->Arith_CTL = 0;
                    theControls->ArithMux_CTL = 0;
                    theControls->LOGIC_CTL = 0;
                    theControls->LogicMux_CTL = 0;
                    theControls->SHIFT_CTL = 0;
                    theControls->CONST_CTL = 0;
                    theControls->CMP_CTL = 0;
                    theControls->ALUMux_CTL = 0;
                    theControls->NZP_WE = 0;
                    theControls->DATA_WE = 0;
                    theControls->Privilege_CTL = 2;
                    printf("\tNOP\n");
                    return 0;
                case 0x4:   //BRn
                    theControls->PCMux_CTL = 0;
                    theControls->rsMux_CTL = 0;
                    theControls->rtMux_CTL = 0;
                    theControls->rdMux_CTL = 0;
                    theControls->regFile_WE = 0;
                    theControls->regInputMux_CTL = 0;
                    theControls->Arith_CTL = 0;
                    theControls->ArithMux_CTL = 0;
                    theControls->LOGIC_CTL = 0;
                    theControls->LogicMux_CTL = 0;
                    theControls->SHIFT_CTL = 0;
                    theControls->CONST_CTL = 0;
                    theControls->CMP_CTL = 0;
                    theControls->ALUMux_CTL = 0;
                    theControls->NZP_WE = 0;
                    theControls->DATA_WE = 0;
                    theControls->Privilege_CTL = 2;
                    printf("\tBRn ");
                    return 0;
                case 0x6:   //BRnz
                    theControls->PCMux_CTL = 0;
                    theControls->rsMux_CTL = 0;
                    theControls->rtMux_CTL = 0;
                    theControls->rdMux_CTL = 0;
                    theControls->regFile_WE = 0;
                    theControls->regInputMux_CTL = 0;
                    theControls->Arith_CTL = 0;
                    theControls->ArithMux_CTL = 0;
                    theControls->LOGIC_CTL = 0;
                    theControls->LogicMux_CTL = 0;
                    theControls->SHIFT_CTL = 0;
                    theControls->CONST_CTL = 0;
                    theControls->CMP_CTL = 0;
                    theControls->ALUMux_CTL = 0;
                    theControls->NZP_WE = 0;
                    theControls->DATA_WE = 0;
                    theControls->Privilege_CTL = 2;
                    printf("\tBRnz ");
                    return 0;
                case 0x5:   //BRnp
                    theControls->PCMux_CTL = 0;
                    theControls->rsMux_CTL = 0;
                    theControls->rtMux_CTL = 0;
                    theControls->rdMux_CTL = 0;
                    theControls->regFile_WE = 0;
                    theControls->regInputMux_CTL = 0;
                    theControls->Arith_CTL = 0;
                    theControls->ArithMux_CTL = 0;
                    theControls->LOGIC_CTL = 0;
                    theControls->LogicMux_CTL = 0;
                    theControls->SHIFT_CTL = 0;
                    theControls->CONST_CTL = 0;
                    theControls->CMP_CTL = 0;
                    theControls->ALUMux_CTL = 0;
                    theControls->NZP_WE = 0;
                    theControls->DATA_WE = 0;
                    theControls->Privilege_CTL = 2;
                    printf("\tBRnp ");
                    return 0;
                case 0x2:   //BRz
                    theControls->PCMux_CTL = 0;
                    theControls->rsMux_CTL = 0;
                    theControls->rtMux_CTL = 0;
                    theControls->rdMux_CTL = 0;
                    theControls->regFile_WE = 0;
                    theControls->regInputMux_CTL = 0;
                    theControls->Arith_CTL = 0;
                    theControls->ArithMux_CTL = 0;
                    theControls->LOGIC_CTL = 0;
                    theControls->LogicMux_CTL = 0;
                    theControls->SHIFT_CTL = 0;
                    theControls->CONST_CTL = 0;
                    theControls->CMP_CTL = 0;
                    theControls->ALUMux_CTL = 0;
                    theControls->NZP_WE = 0;
                    theControls->DATA_WE = 0;
                    theControls->Privilege_CTL = 2;
                    printf("\tBRz ");
                    return 0;
                case 0x3:   //BRzp
                    theControls->PCMux_CTL = 0;
                    theControls->rsMux_CTL = 0;
                    theControls->rtMux_CTL = 0;
                    theControls->rdMux_CTL = 0;
                    theControls->regFile_WE = 0;
                    theControls->regInputMux_CTL = 0;
                    theControls->Arith_CTL = 0;
                    theControls->ArithMux_CTL = 0;
                    theControls->LOGIC_CTL = 0;
                    theControls->LogicMux_CTL = 0;
                    theControls->SHIFT_CTL = 0;
                    theControls->CONST_CTL = 0;
                    theControls->CMP_CTL = 0;
                    theControls->ALUMux_CTL = 0;
                    theControls->NZP_WE = 0;
                    theControls->DATA_WE = 0;
                    theControls->Privilege_CTL = 2;
                    fprintf(stderr,"\tBRzp ");
                    return 0;
                case 0x1:   //BRp
                    theControls->PCMux_CTL = 0;
                    theControls->rsMux_CTL = 0;
                    theControls->rtMux_CTL = 0;
                    theControls->rdMux_CTL = 0;
                    theControls->regFile_WE = 0;
                    theControls->regInputMux_CTL = 0;
                    theControls->Arith_CTL = 0;
                    theControls->ArithMux_CTL = 0;
                    theControls->LOGIC_CTL = 0;
                    theControls->LogicMux_CTL = 0;
                    theControls->SHIFT_CTL = 0;
                    theControls->CONST_CTL = 0;
                    theControls->CMP_CTL = 0;
                    theControls->ALUMux_CTL = 0;
                    theControls->NZP_WE = 0;
                    theControls->DATA_WE = 0;
                    theControls->Privilege_CTL = 2;
                    fprintf(stderr,"\tBRp ");
                    return 0;
                case 0x7:   //BRnzp
                    theControls->PCMux_CTL = 0;
                    theControls->rsMux_CTL = 0;
                    theControls->rtMux_CTL = 0;
                    theControls->rdMux_CTL = 0;
                    theControls->regFile_WE = 0;
                    theControls->regInputMux_CTL = 0;
                    theControls->Arith_CTL = 0;
                    theControls->ArithMux_CTL = 0;
                    theControls->LOGIC_CTL = 0;
                    theControls->LogicMux_CTL = 0;
                    theControls->SHIFT_CTL = 0;
                    theControls->CONST_CTL = 0;
                    theControls->CMP_CTL = 0;
                    theControls->ALUMux_CTL = 0;
                    theControls->NZP_WE = 0;
                    theControls->DATA_WE = 0;
                    theControls->Privilege_CTL = 2;
                    printf("\tBRnzp ");
                    return 0;
                default: //error
                    fprintf(stderr, "Instruction is invalid.");
                    return 1;
            }
        case 0x1:  //arithmetic operation
            switch(INSN_SUBOP(INSN)){
                case 0x0:   //ADD
                    theControls->PCMux_CTL = 1;
                    theControls->rsMux_CTL = 0;
                    theControls->rtMux_CTL = 0;
                    theControls->rdMux_CTL = 0;
                    theControls->regFile_WE = 1;
                    theControls->regInputMux_CTL = 0;
                    theControls->Arith_CTL = 0;
                    theControls->ArithMux_CTL = 0;
                    theControls->LOGIC_CTL = 0;
                    theControls->LogicMux_CTL = 0;
                    theControls->SHIFT_CTL = 0;
                    theControls->CONST_CTL = 0;
                    theControls->CMP_CTL = 0;
                    theControls->ALUMux_CTL = 0;
                    theControls->NZP_WE = 1;
                    theControls->DATA_WE = 0;
                    theControls->Privilege_CTL = 2;
                    printf("\tADD ");
                    return 0;
                case 0x1:   //MUL
                    theControls->PCMux_CTL = 1;
                    theControls->rsMux_CTL = 0;
                    theControls->rtMux_CTL = 0;
                    theControls->rdMux_CTL = 0;
                    theControls->regFile_WE = 1;
                    theControls->regInputMux_CTL = 0;
                    theControls->Arith_CTL = 1;
                    theControls->ArithMux_CTL = 0;
                    theControls->LOGIC_CTL = 0;
                    theControls->LogicMux_CTL = 0;
                    theControls->SHIFT_CTL = 0;
                    theControls->CONST_CTL = 0;
                    theControls->CMP_CTL = 0;
                    theControls->ALUMux_CTL = 0;
                    theControls->NZP_WE = 1;
                    theControls->DATA_WE = 0;
                    theControls->Privilege_CTL = 2;
                    printf("\tMUL ");
                    return 0;
                case 0x2:   //SUB
                    theControls->PCMux_CTL = 1;
                    theControls->rsMux_CTL = 0;
                    theControls->rtMux_CTL = 0;
                    theControls->rdMux_CTL = 0;
                    theControls->regFile_WE = 1;
                    theControls->regInputMux_CTL = 0;
                    theControls->Arith_CTL = 2;
                    theControls->ArithMux_CTL = 0;
                    theControls->LOGIC_CTL = 0;
                    theControls->LogicMux_CTL = 0;
                    theControls->SHIFT_CTL = 0;
                    theControls->CONST_CTL = 0;
                    theControls->CMP_CTL = 0;
                    theControls->ALUMux_CTL = 0;
                    theControls->NZP_WE = 1;
                    theControls->DATA_WE = 0;
                    theControls->Privilege_CTL = 2;
                    printf("\tSUB ");
                    return 0;
                case 0x3:   //DIV
                    theControls->PCMux_CTL = 1;
                    theControls->rsMux_CTL = 0;
                    theControls->rtMux_CTL = 0;
                    theControls->rdMux_CTL = 0;
                    theControls->regFile_WE = 1;
                    theControls->regInputMux_CTL = 0;
                    theControls->Arith_CTL = 3;
                    theControls->ArithMux_CTL = 0;
                    theControls->LOGIC_CTL = 0;
                    theControls->LogicMux_CTL = 0;
                    theControls->SHIFT_CTL = 0;
                    theControls->CONST_CTL = 0;
                    theControls->CMP_CTL = 0;
                    theControls->ALUMux_CTL = 0;
                    theControls->NZP_WE = 1;
                    theControls->DATA_WE = 0;
                    theControls->Privilege_CTL = 2;
                    printf("\tDIV ");
                    return 0;
                default:
                    if(INSN_5(INSN) == 1){ //ADD imm
                        theControls->PCMux_CTL = 1;
                        theControls->rsMux_CTL = 0;
                        theControls->rtMux_CTL = 0;
                        theControls->rdMux_CTL = 0;
                        theControls->regFile_WE = 1;
                        theControls->regInputMux_CTL = 0;
                        theControls->Arith_CTL = 0;
                        theControls->ArithMux_CTL = 1;
                        theControls->LOGIC_CTL = 0;
                        theControls->LogicMux_CTL = 0;
                        theControls->SHIFT_CTL = 0;
                        theControls->CONST_CTL = 0;
                        theControls->CMP_CTL = 0;
                        theControls->ALUMux_CTL = 0;
                        theControls->NZP_WE = 1;
                        theControls->DATA_WE = 0;
                        theControls->Privilege_CTL = 2;
                        printf("\tADDi ");
                        return 0;
                    } else {
                        fprintf(stderr, "Instruction is invalid.");
                        return 1;//error
                    }
            }
        case 0x2: //constant operation
            switch(INSN_8_7(INSN)){
                case 0x0:   //CMP
                    theControls->PCMux_CTL = 1;
                    theControls->rsMux_CTL = 2;
                    theControls->rtMux_CTL = 0;
                    theControls->rdMux_CTL = 0;
                    theControls->regFile_WE = 0;
                    theControls->regInputMux_CTL = 0;
                    theControls->Arith_CTL = 0;
                    theControls->ArithMux_CTL = 0;
                    theControls->LOGIC_CTL = 0;
                    theControls->LogicMux_CTL = 0;
                    theControls->SHIFT_CTL = 0;
                    theControls->CONST_CTL = 0;
                    theControls->CMP_CTL = 0;
                    theControls->ALUMux_CTL = 4;
                    theControls->NZP_WE = 1;
                    theControls->DATA_WE = 0;
                    theControls->Privilege_CTL = 2;
                    printf("\tCMP ");
                    return 0;
                case 0x1:   //CMPU
                    theControls->PCMux_CTL = 1;
                    theControls->rsMux_CTL = 2;
                    theControls->rtMux_CTL = 0;
                    theControls->rdMux_CTL = 0;
                    theControls->regFile_WE = 0;
                    theControls->regInputMux_CTL = 0;
                    theControls->Arith_CTL = 0;
                    theControls->ArithMux_CTL = 0;
                    theControls->LOGIC_CTL = 0;
                    theControls->LogicMux_CTL = 0;
                    theControls->SHIFT_CTL = 0;
                    theControls->CONST_CTL = 0;
                    theControls->CMP_CTL = 1;
                    theControls->ALUMux_CTL = 4;
                    theControls->NZP_WE = 1;
                    theControls->DATA_WE = 0;
                    theControls->Privilege_CTL = 2;
                    printf("\tCMPU ");
                    return 0;
                case 0x2:   //CMPI
                    theControls->PCMux_CTL = 1;
                    theControls->rsMux_CTL = 2;
                    theControls->rtMux_CTL = 0;
                    theControls->rdMux_CTL = 0;
                    theControls->regFile_WE = 0;
                    theControls->regInputMux_CTL = 0;
                    theControls->Arith_CTL = 0;
                    theControls->ArithMux_CTL = 0;
                    theControls->LOGIC_CTL = 0;
                    theControls->LogicMux_CTL = 0;
                    theControls->SHIFT_CTL = 0;
                    theControls->CONST_CTL = 0;
                    theControls->CMP_CTL = 2;
                    theControls->ALUMux_CTL = 4;
                    theControls->NZP_WE = 1;
                    theControls->DATA_WE = 0;
                    theControls->Privilege_CTL = 2;
                    fprintf(stderr,"\tCMPI ");
                    return 0;
                case 0x3:   //CMPIU
                    theControls->PCMux_CTL = 1;
                    theControls->rsMux_CTL = 2;
                    theControls->rtMux_CTL = 0;
                    theControls->rdMux_CTL = 0;
                    theControls->regFile_WE = 0;
                    theControls->regInputMux_CTL = 0;
                    theControls->Arith_CTL = 0;
                    theControls->ArithMux_CTL = 0;
                    theControls->LOGIC_CTL = 0;
                    theControls->LogicMux_CTL = 0;
                    theControls->SHIFT_CTL = 0;
                    theControls->CONST_CTL = 0;
                    theControls->CMP_CTL = 3;
                    theControls->ALUMux_CTL = 4;
                    theControls->NZP_WE = 1;
                    theControls->DATA_WE = 0;
                    theControls->Privilege_CTL = 2;
                    printf("\tCMPIU ");
                    return 0;
                default:    //error
                    fprintf(stderr, "Instruction is invalid.");
                    return 1;//error
            }
        case 0x4:   //JSR operation
            switch(INSN_11(INSN)){
                case 0x0:   //JSRR
                    theControls->PCMux_CTL = 3;
                    theControls->rsMux_CTL = 0;
                    theControls->rtMux_CTL = 0;
                    theControls->rdMux_CTL = 1;
                    theControls->regFile_WE = 1;
                    theControls->regInputMux_CTL = 2;
                    theControls->Arith_CTL = 0;
                    theControls->ArithMux_CTL = 0;
                    theControls->LOGIC_CTL = 0;
                    theControls->LogicMux_CTL = 0;
                    theControls->SHIFT_CTL = 0;
                    theControls->CONST_CTL = 0;
                    theControls->CMP_CTL = 0;
                    theControls->ALUMux_CTL = 0;
                    theControls->NZP_WE = 1;
                    theControls->DATA_WE = 0;
                    theControls->Privilege_CTL = 2;
                    printf("\tJSRR ");
                    return 0;
                case 0x1:   //JSR
                    theControls->PCMux_CTL = 5;
                    theControls->rsMux_CTL = 0;
                    theControls->rtMux_CTL = 0;
                    theControls->rdMux_CTL = 1;
                    theControls->regFile_WE = 1;
                    theControls->regInputMux_CTL = 2;
                    theControls->Arith_CTL = 0;
                    theControls->ArithMux_CTL = 0;
                    theControls->LOGIC_CTL = 0;
                    theControls->LogicMux_CTL = 0;
                    theControls->SHIFT_CTL = 0;
                    theControls->CONST_CTL = 0;
                    theControls->CMP_CTL = 0;
                    theControls->ALUMux_CTL = 0;
                    theControls->NZP_WE = 1;
                    theControls->DATA_WE = 0;
                    theControls->Privilege_CTL = 2;
                    printf("\tJSR ");
                    return 0;
                default:    //error
                    fprintf(stderr, "Instruction is invalid.");
                    return 1;//error
            }
        case 0x5:   //logical operation
            switch(INSN_SUBOP(INSN)){
                case 0x0:   //AND
                    theControls->PCMux_CTL = 1;
                    theControls->rsMux_CTL = 0;
                    theControls->rtMux_CTL = 0;
                    theControls->rdMux_CTL = 0;
                    theControls->regFile_WE = 1;
                    theControls->regInputMux_CTL = 0;
                    theControls->Arith_CTL = 0;
                    theControls->ArithMux_CTL = 0;
                    theControls->LOGIC_CTL = 0;
                    theControls->LogicMux_CTL = 0;
                    theControls->SHIFT_CTL = 0;
                    theControls->CONST_CTL = 0;
                    theControls->CMP_CTL = 0;
                    theControls->ALUMux_CTL = 1;
                    theControls->NZP_WE = 1;
                    theControls->DATA_WE = 0;
                    theControls->Privilege_CTL = 2;
                    printf("\tAND ");
                    return 0;
                case 0x1:   //NOT
                    theControls->PCMux_CTL = 1;
                    theControls->rsMux_CTL = 0;
                    theControls->rtMux_CTL = 0;
                    theControls->rdMux_CTL = 0;
                    theControls->regFile_WE = 1;
                    theControls->regInputMux_CTL = 0;
                    theControls->Arith_CTL = 0;
                    theControls->ArithMux_CTL = 0;
                    theControls->LOGIC_CTL = 1;
                    theControls->LogicMux_CTL = 0;
                    theControls->SHIFT_CTL = 0;
                    theControls->CONST_CTL = 0;
                    theControls->CMP_CTL = 0;
                    theControls->ALUMux_CTL = 1;
                    theControls->NZP_WE = 1;
                    theControls->DATA_WE = 0;
                    theControls->Privilege_CTL = 2;
                    printf("\tNOT ");
                    return 0;
                case 0x2:   //OR
                    theControls->PCMux_CTL = 1;
                    theControls->rsMux_CTL = 0;
                    theControls->rtMux_CTL = 0;
                    theControls->rdMux_CTL = 0;
                    theControls->regFile_WE = 1;
                    theControls->regInputMux_CTL = 0;
                    theControls->Arith_CTL = 0;
                    theControls->ArithMux_CTL = 0;
                    theControls->LOGIC_CTL = 2;
                    theControls->LogicMux_CTL = 0;
                    theControls->SHIFT_CTL = 0;
                    theControls->CONST_CTL = 0;
                    theControls->CMP_CTL = 0;
                    theControls->ALUMux_CTL = 1;
                    theControls->NZP_WE = 1;
                    theControls->DATA_WE = 0;
                    theControls->Privilege_CTL = 2;
                    printf("\tOR ");
                    return 0;
                case 0x3:   //XOR
                    theControls->PCMux_CTL = 1;
                    theControls->rsMux_CTL = 0;
                    theControls->rtMux_CTL = 0;
                    theControls->rdMux_CTL = 0;
                    theControls->regFile_WE = 1;
                    theControls->regInputMux_CTL = 0;
                    theControls->Arith_CTL = 0;
                    theControls->ArithMux_CTL = 0;
                    theControls->LOGIC_CTL = 3;
                    theControls->LogicMux_CTL = 0;
                    theControls->SHIFT_CTL = 0;
                    theControls->CONST_CTL = 0;
                    theControls->CMP_CTL = 0;
                    theControls->ALUMux_CTL = 1;
                    theControls->NZP_WE = 1;
                    theControls->DATA_WE = 0;
                    theControls->Privilege_CTL = 2;
                    printf("\tXOR ");
                    return 0;
                default:
                    if(INSN_12(INSN) == 1){ //AND immediate
                        theControls->PCMux_CTL = 1;
                        theControls->rsMux_CTL = 0;
                        theControls->rtMux_CTL = 0;
                        theControls->rdMux_CTL = 0;
                        theControls->regFile_WE = 1;
                        theControls->regInputMux_CTL = 0;
                        theControls->Arith_CTL = 0;
                        theControls->ArithMux_CTL = 0;
                        theControls->LOGIC_CTL = 0;
                        theControls->LogicMux_CTL = 1;
                        theControls->SHIFT_CTL = 0;
                        theControls->CONST_CTL = 0;
                        theControls->CMP_CTL = 0;
                        theControls->ALUMux_CTL = 1;
                        theControls->NZP_WE = 1;
                        theControls->DATA_WE = 0;
                        theControls->Privilege_CTL = 2;
                        printf("\tANDi ");
                        return 0;
                    } else { //error
                        fprintf(stderr, "Instruction is invalid.");
                        return 1;
                    }
            }
        case 0x6:   //LDR
            theControls->PCMux_CTL = 1;
            theControls->rsMux_CTL = 0;
            theControls->rtMux_CTL = 0;
            theControls->rdMux_CTL = 0;
            theControls->regFile_WE = 1;
            theControls->regInputMux_CTL = 1;
            theControls->Arith_CTL = 0;
            theControls->ArithMux_CTL = 2;
            theControls->LOGIC_CTL = 0;
            theControls->LogicMux_CTL = 0;
            theControls->SHIFT_CTL = 0;
            theControls->CONST_CTL = 0;
            theControls->CMP_CTL = 0;
            theControls->ALUMux_CTL = 0;
            theControls->NZP_WE = 1;
            theControls->DATA_WE = 0;
            theControls->Privilege_CTL = 2;
            printf("\tLDR ");
            return 0;
        case 0x7:   //STR
            theControls->PCMux_CTL = 1;
            theControls->rsMux_CTL = 0;
            theControls->rtMux_CTL = 1;
            theControls->rdMux_CTL = 0;
            theControls->regFile_WE = 0;
            theControls->regInputMux_CTL = 0;
            theControls->Arith_CTL = 0;
            theControls->ArithMux_CTL = 2;
            theControls->LOGIC_CTL = 0;
            theControls->LogicMux_CTL = 0;
            theControls->SHIFT_CTL = 0;
            theControls->CONST_CTL = 0;
            theControls->CMP_CTL = 0;
            theControls->ALUMux_CTL = 0;
            theControls->NZP_WE = 0;
            theControls->DATA_WE = 1;
            theControls->Privilege_CTL = 2;
            printf("\tSTR ");
            return 0;
        case 0x8:   //RTI
            theControls->PCMux_CTL = 3;
            theControls->rsMux_CTL = 1;
            theControls->rtMux_CTL = 0;
            theControls->rdMux_CTL = 0;
            theControls->regFile_WE = 0;
            theControls->regInputMux_CTL = 0;
            theControls->Arith_CTL = 0;
            theControls->ArithMux_CTL = 0;
            theControls->LOGIC_CTL = 0;
            theControls->LogicMux_CTL = 0;
            theControls->SHIFT_CTL = 0;
            theControls->CONST_CTL = 0;
            theControls->CMP_CTL = 0;
            theControls->ALUMux_CTL = 0;
            theControls->NZP_WE = 0;
            theControls->DATA_WE = 0;
            theControls->Privilege_CTL = 0;
            printf("\tRTI\n");
            return 0;
        case 0x9:   //CONST
            theControls->PCMux_CTL = 1;
            theControls->rsMux_CTL = 0;
            theControls->rtMux_CTL = 0;
            theControls->rdMux_CTL = 0;
            theControls->regFile_WE = 1;
            theControls->regInputMux_CTL = 0;
            theControls->Arith_CTL = 0;
            theControls->ArithMux_CTL = 0;
            theControls->LOGIC_CTL = 0;
            theControls->LogicMux_CTL = 0;
            theControls->SHIFT_CTL = 0;
            theControls->CONST_CTL = 0;
            theControls->CMP_CTL = 0;
            theControls->ALUMux_CTL = 3;
            theControls->NZP_WE = 1;
            theControls->DATA_WE = 0;
            theControls->Privilege_CTL = 2;
            printf("\tCONST ");
            return 0;
        case 0xA:   //shift operation
            switch(INSN_5_4(INSN)){
                case 0x0:   //SLL
                    theControls->PCMux_CTL = 1;
                    theControls->rsMux_CTL = 0;
                    theControls->rtMux_CTL = 0;
                    theControls->rdMux_CTL = 0;
                    theControls->regFile_WE = 1;
                    theControls->regInputMux_CTL = 0;
                    theControls->Arith_CTL = 0;
                    theControls->ArithMux_CTL = 0;
                    theControls->LOGIC_CTL = 0;
                    theControls->LogicMux_CTL = 0;
                    theControls->SHIFT_CTL = 0;
                    theControls->CONST_CTL = 0;
                    theControls->CMP_CTL = 0;
                    theControls->ALUMux_CTL = 2;
                    theControls->NZP_WE = 1;
                    theControls->DATA_WE = 0;
                    theControls->Privilege_CTL = 2;
                    printf("\tSLL ");
                    return 0;
                case 0x1:   //SRA
                    theControls->PCMux_CTL = 1;
                    theControls->rsMux_CTL = 0;
                    theControls->rtMux_CTL = 0;
                    theControls->rdMux_CTL = 0;
                    theControls->regFile_WE = 1;
                    theControls->regInputMux_CTL = 0;
                    theControls->Arith_CTL = 0;
                    theControls->ArithMux_CTL = 0;
                    theControls->LOGIC_CTL = 0;
                    theControls->LogicMux_CTL = 0;
                    theControls->SHIFT_CTL = 1;
                    theControls->CONST_CTL = 0;
                    theControls->CMP_CTL = 0;
                    theControls->ALUMux_CTL = 2;
                    theControls->NZP_WE = 1;
                    theControls->DATA_WE = 0;
                    theControls->Privilege_CTL = 2;
                    printf("\tSRA ");
                    return 0;
                case 0x2:   //SRL
                    theControls->PCMux_CTL = 1;
                    theControls->rsMux_CTL = 0;
                    theControls->rtMux_CTL = 0;
                    theControls->rdMux_CTL = 0;
                    theControls->regFile_WE = 1;
                    theControls->regInputMux_CTL = 0;
                    theControls->Arith_CTL = 0;
                    theControls->ArithMux_CTL = 0;
                    theControls->LOGIC_CTL = 0;
                    theControls->LogicMux_CTL = 0;
                    theControls->SHIFT_CTL = 0;
                    theControls->CONST_CTL = 0;
                    theControls->CMP_CTL = 0;
                    theControls->ALUMux_CTL = 2;
                    theControls->NZP_WE = 1;
                    theControls->DATA_WE = 0;
                    theControls->Privilege_CTL = 2;
                    printf("\tSRL ");
                    return 0;
                case 0x3:   //MOD
                    theControls->PCMux_CTL = 1;
                    theControls->rsMux_CTL = 0;
                    theControls->rtMux_CTL = 0;
                    theControls->rdMux_CTL = 0;
                    theControls->regFile_WE = 1;
                    theControls->regInputMux_CTL = 0;
                    theControls->Arith_CTL = 4;
                    theControls->ArithMux_CTL = 0;
                    theControls->LOGIC_CTL = 0;
                    theControls->LogicMux_CTL = 0;
                    theControls->SHIFT_CTL = 0;
                    theControls->CONST_CTL = 0;
                    theControls->CMP_CTL = 0;
                    theControls->ALUMux_CTL = 0;
                    theControls->NZP_WE = 1;
                    theControls->DATA_WE = 0;
                    theControls->Privilege_CTL = 2;
                    printf("\tMOD ");
                    return 0;
                default: //error
                    fprintf(stderr, "Instruction is invalid.");
                    return 1;
            }
        case 0xC:   //JMP operation
            switch(INSN_11(INSN)){
                case 0x0:   //JMPR
                    theControls->PCMux_CTL = 3;
                    theControls->rsMux_CTL = 0;
                    theControls->rtMux_CTL = 0;
                    theControls->rdMux_CTL = 0;
                    theControls->regFile_WE = 0;
                    theControls->regInputMux_CTL = 0;
                    theControls->Arith_CTL = 0;
                    theControls->ArithMux_CTL = 0;
                    theControls->LOGIC_CTL = 0;
                    theControls->LogicMux_CTL = 0;
                    theControls->SHIFT_CTL = 0;
                    theControls->CONST_CTL = 0;
                    theControls->CMP_CTL = 0;
                    theControls->ALUMux_CTL = 0;
                    theControls->NZP_WE = 0;
                    theControls->DATA_WE = 0;
                    theControls->Privilege_CTL = 2;
                    printf("\tJMPR ");
                    return 0;
                case 0x1:   //JMP
                    theControls->PCMux_CTL = 2;
                    theControls->rsMux_CTL = 0;
                    theControls->rtMux_CTL = 0;
                    theControls->rdMux_CTL = 0;
                    theControls->regFile_WE = 0;
                    theControls->regInputMux_CTL = 0;
                    theControls->Arith_CTL = 0;
                    theControls->ArithMux_CTL = 0;
                    theControls->LOGIC_CTL = 0;
                    theControls->LogicMux_CTL = 0;
                    theControls->SHIFT_CTL = 0;
                    theControls->CONST_CTL = 0;
                    theControls->CMP_CTL = 0;
                    theControls->ALUMux_CTL = 0;
                    theControls->NZP_WE = 0;
                    theControls->DATA_WE = 0;
                    theControls->Privilege_CTL = 2;
                    fprintf(stderr,"\tJMP ");
                    return 0;
                default:    //error
                    fprintf(stderr, "Instruction is invalid.");
                    return 1;
                    
            }
        case 0xD:   //HICONST
            theControls->PCMux_CTL = 1;
            theControls->rsMux_CTL = 2;
            theControls->rtMux_CTL = 0;
            theControls->rdMux_CTL = 0;
            theControls->regFile_WE = 1;
            theControls->regInputMux_CTL = 0;
            theControls->Arith_CTL = 0;
            theControls->ArithMux_CTL = 0;
            theControls->LOGIC_CTL = 0;
            theControls->LogicMux_CTL = 0;
            theControls->SHIFT_CTL = 0;
            theControls->CONST_CTL = 1;
            theControls->CMP_CTL = 0;
            theControls->ALUMux_CTL = 3;
            theControls->NZP_WE = 1;
            theControls->DATA_WE = 0;
            theControls->Privilege_CTL = 2;
            printf("\tHICONST ");
            return 0;
        case 0xF:   //TRAP
            theControls->PCMux_CTL = 4;
            theControls->rsMux_CTL = 0;
            theControls->rtMux_CTL = 0;
            theControls->rdMux_CTL = 1;
            theControls->regFile_WE = 1;
            theControls->regInputMux_CTL = 2;
            theControls->Arith_CTL = 0;
            theControls->ArithMux_CTL = 0;
            theControls->LOGIC_CTL = 0;
            theControls->LogicMux_CTL = 0;
            theControls->SHIFT_CTL = 0;
            theControls->CONST_CTL = 0;
            theControls->CMP_CTL = 0;
            theControls->ALUMux_CTL = 0;
            theControls->NZP_WE = 1;
            theControls->DATA_WE = 0;
            theControls->Privilege_CTL = 1;
            fprintf(stderr,"\tTRAP %i\n",UIMM8(INSN));
            return 0;
        default:
            fprintf(stderr, "Instruction is invalid.");
            return 1;
    }
}

int SimulateDatapath(ControlSignals *theControls, MachineState *theMachineState, DatapathSignals *theDatapath){
    unsigned short int currInst = theMachineState->memory[theMachineState->PC];
    short int imm5 = UIMM5(currInst)<<11;
    imm5 = imm5 >> 11;
    
    short int imm6 = UIMM6(currInst)<<10;
    imm6 = imm6 >> 10;
    
    short int imm7 = UIMM7(currInst)<<9;
    imm7 = imm7 >> 9;
    
    short int imm9 = UIMM9(currInst)<<7;
    imm9 = imm9 >> 7;
    
    short int imm11 = UIMM11(currInst)<<5;
    imm11 = imm11 >> 5;
    
    short int compEvalSigned;
    unsigned short int compEvalUnsigned;
    short int compEvalSignedImm;
    unsigned short int compEvalUnsignedUImm;
    
    
    switch(theControls->rsMux_CTL){
        case 0x0:
            theDatapath->RS = theMachineState->R[INSN_8_6(currInst)];
            break;
        case 0x1:
            theDatapath->RS = theMachineState->R[0x7];
            break;
        case 0x2:
            theDatapath->RS = theMachineState->R[INSN_11_9(currInst)];
            break;
    }
    
    switch(theControls->rtMux_CTL){
        case 0x0:
            theDatapath->RT = theMachineState->R[INSN_2_0(currInst)];
            break;
        case 0x1:
            theDatapath->RT = theMachineState->R[INSN_11_9(currInst)];
            break;
    }
    
    
    if(((theControls->regFile_WE == 1) && (theControls->regInputMux_CTL == 0)) || (theControls->ALUMux_CTL == 4)){
        short int aluIn = 1; //default value is 1 in case the instruction is a division and aluIn is somehow not changed
        
        switch(theControls->ArithMux_CTL){
            case 0: //ALU input is RT
                aluIn = theDatapath-> RT;
                break;
            case 1: //ALU input is SEXT(IMM5)
                aluIn = imm5;
                break;
            case 2: //ALU input is SEXT(IMM6)
                aluIn = imm6;
                break;
        }
        
        switch(theControls->ALUMux_CTL){
            case 0x0:   //arithmetic output
                printf("R%i R%i ",INSN_11_9(currInst), INSN_8_6(currInst));
                switch(theControls->Arith_CTL){
                    case 0x0:   //addition
                        if(theControls->ArithMux_CTL == 1){ //add immediate
                            theDatapath->ArithmeticOps = ((short)theDatapath->RS) + imm5;
                            printf("%i\n",imm5);
                        } else { //register add
                            theDatapath->ArithmeticOps = ((short)theDatapath->RS) + aluIn;
                            printf("R%i\n",INSN_2_0(currInst));
                        }
                        break;
                    case 0x1:   //multiply
                        theDatapath->ArithmeticOps = theDatapath->RS * aluIn;
                        printf("R%i\n",INSN_2_0(currInst));
                        break;
                    case 0x2:   //subtract
                        theDatapath->ArithmeticOps = (theDatapath->RS) - (unsigned)aluIn;
                        printf("R%i\n",INSN_2_0(currInst));
                        break;
                    case 0x3:   //divide
                        theDatapath->ArithmeticOps = theDatapath->RS / aluIn;
                        printf("R%i\n",INSN_2_0(currInst));
                        break;
                    case 0x4:   //modulus
                        theDatapath->ArithmeticOps = theDatapath->RS % aluIn;
                        printf("R%i\n",INSN_2_0(currInst));
                        break;
                }
                theDatapath->ALUMux = theDatapath->ArithmeticOps;
                break;
            case 0x1:   //logical output
                printf("R%i R%i",INSN_11_9(currInst), INSN_8_6(currInst));
                switch(theControls->LOGIC_CTL){
                    case 0x0:   //AND
                        if(INSN_5(currInst) == 1){   //AND immediate
                            theDatapath->LogicalOps = theDatapath->RS & imm5;
                            printf(" %i\n",imm5);
                        } else {
                            theDatapath->LogicalOps = theDatapath->RS & aluIn;
                            printf(" R%i\n",INSN_2_0(currInst));
                        }
                        break;
                    case 0x1:   //NOT
                        theDatapath->LogicalOps = ~theDatapath->RS;
                        printf("\n");
                        break;
                    case 0x2:   //OR
                        theDatapath->LogicalOps = theDatapath->RS | aluIn;
                        printf(" R%i\n",INSN_2_0(currInst));
                        break;
                    case 0x3:   //XOR
                        theDatapath->LogicalOps = theDatapath->RS ^ aluIn;
                        printf(" R%i\n",INSN_2_0(currInst));
                        break;
                }
                theDatapath->ALUMux = theDatapath->LogicalOps;
                break;
            case 0x2:   //shifter output
                printf("R%i R%i %i\n",INSN_11_9(currInst), INSN_8_6(currInst), UIMM4(currInst));
                switch(theControls->SHIFT_CTL){
                    case 0x0:   //SLL
                        theDatapath->Shifter = theDatapath->RS << UIMM4(currInst);
                        break;
                    case 0x1:    //SRA
                        theDatapath->Shifter = (unsigned short)((short)theDatapath->RS >> UIMM4(currInst));
                        break;
                    case 0x2:   //SRL
                        theDatapath->Shifter = theDatapath->RS >> UIMM4(currInst);
                        break;
                }
                theDatapath->ALUMux = theDatapath->Shifter;
                break;
            case 0x3:   //constants output
                switch(theControls->CONST_CTL){
                    case 0x0:   //SEXT(IMM9)
                        theDatapath->Constants = imm9;
                        printf("R%i %i\n",INSN_11_9(currInst), imm9);
                        break;
                    case 0x1:   //(RS & xFF) | (UIMM8 <<8)
                        theDatapath->Constants = ((theDatapath->RS & 0xFF) | (UIMM8(currInst)<<8)); //HICONST
                        printf("R%i, %i\n",INSN_11_9(currInst),UIMM8(currInst));
                        break;
                }
                theDatapath->ALUMux = theDatapath->Constants;
                break;
            case 0x4:   //comparator output
                compEvalSigned = ((short)theDatapath->RS)-(short)aluIn;
                compEvalUnsigned = ((unsigned)theDatapath->RS)-(unsigned)aluIn;
                compEvalSignedImm = ((short)theDatapath->RS)-imm7;
                compEvalUnsignedUImm = ((unsigned)theDatapath->RS)-UIMM7(currInst);
                
                fprintf(stderr,"R%i ",INSN_11_9(currInst));
                switch(theControls->CMP_CTL){
                    case 0x0:   //signed CC
                        printf("R%i\n",INSN_2_0(currInst));
                        if(compEvalSigned == 0){
                            theDatapath->Comparator = 0;
                        } else {
                            if(compEvalSigned < 0){
                                theDatapath->Comparator = -1;
                            } else {
                                if(compEvalSigned > 0){
                                    theDatapath->Comparator = 1;
                                }
                            }
                        }
                        break;
                    case 0x1:   //unsigned CC
                        printf("R%i\n",INSN_2_0(currInst));
                        if(compEvalUnsigned == 0){
                            theDatapath->Comparator = 0;
                        } else {
                            if(compEvalUnsigned < 0){
                                theDatapath->Comparator = -1;
                            } else {
                                if(compEvalUnsigned > 0){
                                    theDatapath->Comparator = 1;
                                }
                            }
                        }
                        break;
                    case 0x2:   //signed CC immediate
                        printf("%i\n",imm7);
                        if(compEvalSignedImm == 0){
                            theDatapath->Comparator = 0;
                        } else {
                            if(compEvalSignedImm < 0){
                                theDatapath->Comparator = -1;
                            } else {
                                if(compEvalSignedImm > 0){
                                    theDatapath->Comparator = 1;
                                }
                            }
                        }
                        break;
                    case 0x3:   //unsigned CC immediate
                        printf("%i\n",imm7);
                        if(compEvalUnsignedUImm == 0){
                            theDatapath->Comparator = 0;
                        } else {
                            if(compEvalUnsignedUImm < 0){
                                theDatapath->Comparator = -1;
                            } else {
                                if(compEvalUnsignedUImm > 0){
                                    theDatapath->Comparator = 1;
                                }
                            }
                        }
                        break;
                }
                theDatapath->ALUMux = theDatapath->Comparator;
                break;
        }
    }
    
    switch(theControls->regInputMux_CTL){
        case 0x0:   //write input = ALU output
            theDatapath->regInputMux = theDatapath->ALUMux;
            break;
        case 0x1:   //write input = contents of memory at dmem[RS + IMM6]
            theDatapath->regInputMux = (short)theMachineState->memory[theDatapath->RS + imm6];
            printf("R%i R%i %i\n", INSN_11_9(currInst), INSN_8_6(currInst), imm6);
            break;
        case 0x2:   //write input = PC + 1
            theDatapath->regInputMux = theMachineState->PC+1;
            break;
    }
    
    switch(theControls->PCMux_CTL){
        case 0x0:   //Next PC based on NZP and branch instruction
            printf("%i\n",imm9);
            switch(INSN_11_9(currInst)){
                case 0x0:
                    theDatapath->PCMux = theMachineState->PC+1;
                    break;
                default:
                    if((INSN_11_9(currInst) & NZP(theMachineState->PSR)) != 0x0){
                        theDatapath->PCMux = theMachineState->PC + 1 + imm9;
                    } else {
                        theDatapath->PCMux = theMachineState->PC+1;
                    }
                    break;
            }
            break;
        case 0x1:   //PC = PC + 1
            theDatapath->PCMux = theMachineState->PC + 1;
            break;
        case 0x2:   //PC = PC + 1 + SEXT(IMM11)
            fprintf(stderr,"%i\n",imm11);
            theDatapath->PCMux = theMachineState->PC + 1 + imm11;
            
            break;
        case 0x3:   //PC = RS
            theDatapath->PCMux = theDatapath->RS;
            break;
        case 0x4:   //PC = (0x8000 | UIMM8)
            theDatapath->PCMux = 0x8000 | UIMM8(currInst);
            break;
        case 0x5:   //PC = (PC & 0x8000) | (IMM11 <<4)
            theDatapath->PCMux = ((theMachineState->PC & 0x8000) | (imm11 << 4));
            break;
    }
    return 0;
}

int UpdateMachineState(ControlSignals *theControls, MachineState *theMachineState, DatapathSignals *theDatapath){
    if((theMachineState -> PC >= 0x4000) && (theMachineState -> PC < 0x8000)){ //in user data
        fprintf(stderr, "Attempted to execute at location %hx in user data block.\n", theMachineState->PC);
        return 1;
    }
    
    if(theMachineState -> PC >= 0xA000 && theMachineState -> PC <= 0xFFFF){ //in OS data
        fprintf(stderr, "Attempted to execute at location %hx in OS data block.\n", theMachineState->PC);
        return 1;
    }
    
    if((theMachineState -> PC >= 0x8000 && theMachineState -> PC <= 0xA000) && ((theMachineState->PSR >> 15) != 0x1)){ //in OS code with privilege bit = 0
        fprintf(stderr, "Attempted to execute at location %hx in OS code block without permission.\n", theMachineState->PC);
        return 3;
    }
    
    DecodeCurrentInstruction(theMachineState->memory[theMachineState->PC], theControls);
    SimulateDatapath(theControls, theMachineState, theDatapath);
    
    unsigned short int rd = 0;
    unsigned short int currInst = theMachineState->memory[theMachineState->PC];
    short int imm6 = UIMM6(currInst)<<10;
    imm6 = imm6 >> 10;
    
    unsigned short int dataRead = theDatapath->RS + imm6;
    
    //Write to the specified register in rd.addr if necessary
    switch(theControls->regFile_WE){ //write to the specified register if necessary from regInputMux
        case 0x0:
            break;
        case 0x1:
            if(theControls->regInputMux_CTL == 1){
                if((dataRead >= 0x0)&&(dataRead <0x4000)){
                    fprintf(stderr, "Tried to read user code section as data at address %x.\n", dataRead);
                    return 2;
                }
                
                if((dataRead >= 0x8000)&&(dataRead < 0xA000)){
                    fprintf(stderr, "Tried to read OS code section as data at address %x.\n", dataRead);
                    return 2;
                }
                
                if((dataRead >= 0xA000)&&((theMachineState->PSR >> 15) != 0x1)){
                    fprintf(stderr, "Tried to read OS data section without permission at address %x.\n", dataRead);
                    return 3;
                }
                
            }
            switch(theControls->rdMux_CTL){
                case 0x0:   //rd.addr = I[11:9]
                    rd = INSN_11_9(currInst);
                    break;
                case 0x1:   //rd.addr = 0x07
                    rd = 0x7;
                    break;
            }
			theMachineState->R[rd] = theDatapath->regInputMux;
            break;
    }
    
    //Write to data memory if necessary
    switch(theControls->DATA_WE){ //write to dmem[RS + SEXT(IMM6)] from RT if necessary
        case 0x0:
            break;
        case 0x1:
            if(dataRead < 0x4000 || ((dataRead >= 0x8000) && (dataRead < 0xA000))){
                fprintf(stderr, "Tried to write to code section as data at address %x.\n", dataRead);
                return 2;
            }
            
            if((dataRead >= 0xA000) && (dataRead <= 0xFFFF) && ((theMachineState->PSR >> 15) != 0x1)){
                fprintf(stderr, "Tried to write to OS data without permission at address %x.\n", dataRead);
                return 2;
            }
            theMachineState->memory[dataRead] = theDatapath->RT;
            printf("Wrote %x to %x\n", theDatapath->RT,dataRead);
            printf("R%i R%i %i\n", INSN_11_9(currInst), INSN_8_6(currInst), imm6);
            break;
    }
    
    //Update PC
    theMachineState->PC = theDatapath->PCMux; //update PC
    
    //Update NZP
    if(theControls->regFile_WE == 1){
        if((short)theDatapath->regInputMux == 0){
            theMachineState->PSR = ((theMachineState->PSR & 0xFFF8) | 0x2);
        }
        
        if((short)theDatapath->regInputMux < 0){
            theMachineState->PSR = ((theMachineState->PSR & 0xFFF8) | 0x4);
        }
        
        if((short)theDatapath->regInputMux > 0){
            theMachineState->PSR = ((theMachineState->PSR & 0xFFF8) | 0x1);
        }
    } else if(theControls->ALUMux_CTL == 4){
        if((short)theDatapath->Comparator == 0){
            theMachineState->PSR = ((theMachineState->PSR & 0xFFF8) | 0x2);
        }
        
        if((short)theDatapath->Comparator < 0){
            theMachineState->PSR = ((theMachineState->PSR & 0xFFF8) | 0x4);
        }
        
        if((short)theDatapath->Comparator > 0){
            theMachineState->PSR = ((theMachineState->PSR & 0xFFF8) | 0x1);
        }
    }
    
    //Update privilege bit
    if(theControls->Privilege_CTL == 0){
        theMachineState->PSR = (theMachineState->PSR & 0x7FFF); //set PSR[15] to 0, leave lower bits alone
    }
    
    if(theControls->Privilege_CTL == 1){
        theMachineState->PSR = (theMachineState->PSR | 0x8000); //set PSR[15] to 1, leave lower bits alone
    }
    
    return 0;
}

void Reset(MachineState *theMachineState){
    theMachineState -> PC = 0x8200;  //reset PC
    theMachineState -> PSR = 0x8002; //reset PSR
    for(int i = 0; i < 8; i++){
        theMachineState->R[i] = 0; //reset registers
    }
    
    for(int i = 0; i < 65535; i++){ //reset memory
        theMachineState->memory[i] = 0; //fill all locations with a NOP instruction
    }
    theMachineState->memory[0x3FFF] = 0xF0FF; //place TRAP to 0x80FF in the final word in user code region
}
